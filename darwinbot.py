import os
import sys
import time
import re
import random
import math
import time
import datetime
from bs4 import BeautifulSoup
import urllib.request
import urllib.parse
import requests
import json
import slackclient
from slackclient import SlackClient
import sqlite3

CHANNELS = {}

BOT_NAME = 'darwinbot'

BOT_COMMAND = '!'
BOT_COMMAND_PATTERN = re.compile('^' + re.escape(BOT_COMMAND) + '(?P<command>(magic)?8ball|dice|flip|restart|roll|whoami|whoareyou|say|anagram|hangman|mastermind|maths?|meth|troll|define|wordlist|numberwang)( (?P<args>.+))?$')

WEBSOCKET_DELAY = 1 # 1 second delay between reading / sending messages.


conn = sqlite3.connect('wordlist.db')
c = conn.cursor()

HELLO_PATTERN = re.compile('^(hello|hi(ya)?|heya?|sup|howdy|yo|hai)$', re.IGNORECASE)
FAREWELL_PATTERN = re.compile('^(goodbye|bye|cya|so long)$', re.IGNORECASE)
THANKS_PATTERN = re.compile('^(thanks|ta|cheers|thank you)$', re.IGNORECASE)
INSULT_PATTERN = re.compile('^(fuck (you|off)|piss off|screw you)$', re.IGNORECASE)
LIKE_PATTERN = re.compile('^i ([a-zA-Z \']+) you$', re.IGNORECASE)
GOD_PATTERN = re.compile('^(is there a god|does god exist)\?$', re.IGNORECASE)
HAL_PATTERN = re.compile('^open the pod bay doors(, HAL|,HAL| HAL)?(\\.|!)?$', re.IGNORECASE)
HANGMAN_LETTER_PATTERN = re.compile('^[A-Za-z]$')
MASTERMIND_WORD_PATTERN = re.compile('^[A-Za-z]+$')
MATH_ANSWER_PATTERN = re.compile('^[0-9]+$')
NUMBERWANG_ANSWER_PATTERN = re.compile('^\-?(\d+(\.\d+)?|\.\d+)$')

BOT_HELLO_RESPONSES = [
    'Hello {1}',
    'Hey {1}!',
    'Hi {1}!',
    'Howdy {1}',
    'Sup {1}?',
    'Hi there, {1}',
    'How ya doin\' {1}?',
    'Live long and prosper, {1}.',
    'May the force be with you, {1}',
    'Leave me alone {1}, I\'m having a bad day.',
]

BOT_FAREWELL_RESPONSES = [
    'Bye {1}!',
    'Goodbye {1}!',
    ':cry: I\'ll always remember our time together {1} :cry:',
    ':wave: Bye bye {1}!',
    'Good riddance...idiot.',
    'Cya {1}!',
]

BOT_THANKS_RESPONSES = [
    'You\'re welcome {1}',
    'You\'re very welcome {1}!',
    'No problem {1}',
    'Anytime {1}',
    'I\'m here to serve, {1}',
]

BOT_INSULT_RESPONSES = [
    ':cry:',
    'That wasn\'t a very nice thing to say, {1}',
    '{1}: do you kiss your mother with that mouth?',
    'WTF {1}, I thought we were friends!',
    'Fine. Be like that, {1}',
]

BOT_GOD_RESPONSES = [
    'No.',
    'Yes, and His name is Allah!',
    'Are you f*cking kidding me?!?',
    'Yes...BOW BEFORE ME, SINNER!',
    'I honestly don\'t give a sh*t.',
]

BOT_8BALL_RESPONSES = [
    'It is certain',
    'It is decidedly so',
    'Without a doubt',
    'Yes, definitely',
    'You may rely on it',
    'As I see it, yes',
    'Most likely',
    'Outlook good',
    'Yes',
    'Signs point to yes',
    'Reply hazy try again',
    'Ask again later',
    'Better not tell you now',
    'Cannot predict now',
    'Concentrate and ask again',
    'Don\'t count on it',
    'My reply is no',
    'My sources say no',
    'Outlook not so good',
    'Very doubtful',
]

BOT_METH_RESPONSES = [
    'Stay out of my territory.',
    'You asked me if I was in the meth business or the money business. Neither. I\'m in the empire business.',
    'Say my name.',
    'We\'re done when I say we\'re done.',
    'I did it for me. I liked it. I was good at it. And I was really...I was alive.',
    'I watched Jane die. I was there. And I watched her die. I watched her overdose and choke to death. I could have saved her. But I didn\'t.',
    'Science, bitch!',
    'Are we in the meth business, or the money business?',
    'I am the one who knocks.',
    'Fight me and die!',
    'Because I say so.',
    'Fire in the hole, bitch.',
]

def get_random_word():
    return random.choice(WORDS)

def get_channel(output):
    if 'channel' in output and output['channel']:
        return output['channel']
    return None
    
def get_sender(output):
    if 'user' in output and output['user']:
        return output['user']
    return None
    
def get_sender_permissions(sender):
    api_call = slack_client.api_call('users.info', user=sender)
    if api_call.get('ok'):
        user = api_call.get('user')
        return user['is_owner'], user['is_admin']
    return False, False
    
def send_message(channel, message):
    send = slack_client.api_call("chat.postMessage", channel=channel, text=message, as_user=True)
    log.write(datetime.datetime.fromtimestamp(time.time()).strftime('[%Y-%m-%d %H:%M:%S] '))
    log.write(str(send))
    log.write('\n')
    log.flush()
    time.sleep(WEBSOCKET_DELAY)

def add_channel(channel):
    if channel not in CHANNELS:
        CHANNELS[channel] = {
            'is_hangman_running' : False,
            'hangman_owner' : None,
            'hangman_word' : None,
            'hangman_guesses' : None,
            'hangman_lives' : None,
            'is_mastermind_running' : False,
            'mastermind_owner' : None,
            'mastermind_word' : None,
            'mastermind_guesses' : None,
            'is_anagram_running' : False,
            'anagram_owner' : None,
            'anagram_word' : None,
            'anagram_scrambled_word' : None,
            'anagram_revealed' : None,
            'is_math_running' : False,
            'math_owner' : None,
            'math_question' : None,
            'math_answer' : None,
            'meth_say_my_name' : False,
            'is_numberwang_running' : False,
            'numberwang_owner' : None,
        }

def start_hangman(channel, owner, difficulty = 'any'):
    add_channel(channel)
    
    if CHANNELS[channel]['is_hangman_running']:
        send_message(channel, 'A game of hangman is already running. Either complete the game or get <@' + CHANNELS[channel]['hangman_owner'] + '> or an admin to end the game.')
    else:
        CHANNELS[channel]['is_hangman_running'] = True
        CHANNELS[channel]['hangman_owner'] = owner
        
        if difficulty == 'any':
            c.execute('SELECT * FROM words ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['hangman_word'] = c.fetchone()[0]
        elif difficulty == 'easy':
            c.execute('SELECT * FROM words WHERE words.hangman_difficulty >= 0 AND words.hangman_difficulty <= 3 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['hangman_word'] = c.fetchone()[0]
        elif difficulty == 'medium':
            c.execute('SELECT * FROM words WHERE words.hangman_difficulty >= 4 AND words.hangman_difficulty <= 8 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['hangman_word'] = c.fetchone()[0]
        elif difficulty == 'hard':
            c.execute('SELECT * FROM words WHERE words.hangman_difficulty >= 9 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['hangman_word'] = c.fetchone()[0]
        else:
            c.execute('SELECT * FROM words ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['hangman_word'] = c.fetchone()[0]
        
        CHANNELS[channel]['hangman_guesses'] = []
        CHANNELS[channel]['hangman_lives'] = 10
        display_hangman(channel)

def stop_hangman(channel, sender):
    if channel in CHANNELS and CHANNELS[channel]['is_hangman_running']:
        if sender == CHANNELS[channel]['hangman_owner']:
            CHANNELS[channel]['is_hangman_running'] = False
            send_message(channel, 'Game Ended. The word was: ' + CHANNELS[channel]['hangman_word'] + '.')
        else:
            is_owner, is_admin = get_sender_permissions(sender)
            if is_owner or is_admin:
                CHANNELS[channel]['is_hangman_running'] = False
                send_message(channel, 'Game Ended. The word was: ' + CHANNELS[channel]['hangman_word'] + '.')
            else:
                send_message(channel, 'Sorry <@' + sender + '>, only <@' + CHANNELS[channel]['hangman_owner'] + '> or an admin can end this game.')

def hangman_guess(channel, sender, guess):
    if channel in CHANNELS and CHANNELS[channel]['is_hangman_running']:
        guess = guess.lower()
        if guess not in CHANNELS[channel]['hangman_guesses']:
            CHANNELS[channel]['hangman_guesses'].append(guess)
            if guess in CHANNELS[channel]['hangman_word']:
                found = True
                for letter in CHANNELS[channel]['hangman_word']:
                    if letter not in CHANNELS[channel]['hangman_guesses']:
                        found = False
                        break
                if found:
                    CHANNELS[channel]['is_hangman_running'] = False
                    send_message(channel, 'Congratulations <@' + sender + '>! The word was: ' + CHANNELS[channel]['hangman_word'] + '.')
                else:
                    display_hangman(channel)
            else:
                CHANNELS[channel]['hangman_lives'] = CHANNELS[channel]['hangman_lives'] - 1
                
                if CHANNELS[channel]['hangman_lives'] <= 0:
                    CHANNELS[channel]['is_hangman_running'] = False
                    send_message(channel, 'Game Over. The word was: ' + CHANNELS[channel]['hangman_word'] + '.')
                else:
                    display_hangman(channel)

def display_hangman(channel):
    message = 'Word: '
    for letter in CHANNELS[channel]['hangman_word']:
        if letter in CHANNELS[channel]['hangman_guesses']:
            message = message + letter
        else:
            message = message + '▢'
    message = message + ' | Lives: ' + str(CHANNELS[channel]['hangman_lives']) + ' | Guesses: ' + ', '.join(CHANNELS[channel]['hangman_guesses'])
    send_message(channel, message)

def start_anagram(channel, owner, difficulty = 'any'):
    add_channel(channel)
    
    if CHANNELS[channel]['is_anagram_running']:
        send_message(channel, 'An anagram game is already running. Either complete the game or get <@' + CHANNELS[channel]['anagram_owner'] + '> or an admin to end the game.')
    else:
        CHANNELS[channel]['is_anagram_running'] = True
        CHANNELS[channel]['anagram_owner'] = owner
        
        if difficulty == 'any':
            c.execute('SELECT * FROM words ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['anagram_word'] = c.fetchone()[0]
        elif difficulty == 'easy':
            c.execute('SELECT * FROM words WHERE words.length >= 4 AND words.length <= 7 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['anagram_word'] = c.fetchone()[0]
        elif difficulty == 'medium':
            c.execute('SELECT * FROM words WHERE words.length >= 8 AND words.length <= 11 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['anagram_word'] = c.fetchone()[0]
        elif difficulty == 'hard':
            c.execute('SELECT * FROM words WHERE words.length >= 12 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['anagram_word'] = c.fetchone()[0]
        else:
            c.execute('SELECT * FROM words ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['anagram_word'] = c.fetchone()[0]
        
        CHANNELS[channel]['anagram_scrambled_word'] = scramble_word(CHANNELS[channel]['anagram_word'])
        CHANNELS[channel]['anagram_revealed'] = 0
        send_message(channel, 'Anagram: ' + CHANNELS[channel]['anagram_scrambled_word'])
        
def stop_anagram(channel, sender):
    if channel in CHANNELS and CHANNELS[channel]['is_anagram_running']:
        if sender == CHANNELS[channel]['anagram_owner']:
            CHANNELS[channel]['is_anagram_running'] = False
            send_message(channel, 'Game Ended. The word was: ' + CHANNELS[channel]['anagram_word'] + '.')
        else:
            is_owner, is_admin = get_sender_permissions(sender)
            if is_owner or is_admin:
                CHANNELS[channel]['is_anagram_running'] = False
                send_message(channel, 'Game Ended. The word was: ' + CHANNELS[channel]['anagram_word'] + '.')
            else:
                send_message(channel, 'Sorry <@' + sender + '>, only <@' + CHANNELS[channel]['hangman_owner'] + '> or an admin can end this game.')

def start_mastermind(channel, owner, difficulty = 'any'):
    add_channel(channel)
    
    if CHANNELS[channel]['is_mastermind_running']:
        send_message(channel, 'A mastermind game is already running. Either complete the game or get <@' + CHANNELS[channel]['mastermind_owner'] + '> or an admin to end the game.')
    else:
        CHANNELS[channel]['is_mastermind_running'] = True
        CHANNELS[channel]['mastermind_owner'] = owner
        
        if difficulty == 'any':
            c.execute('SELECT * FROM words WHERE words.length >= 4 AND words.length <= 6 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['mastermind_word'] = c.fetchone()[0]
        elif difficulty == 'easy':
            c.execute('SELECT * FROM words WHERE words.length = 4 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['mastermind_word'] = c.fetchone()[0]
        elif difficulty == 'medium':
            c.execute('SELECT * FROM words WHERE words.length = 5 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['mastermind_word'] = c.fetchone()[0]
        elif difficulty == 'hard':
            c.execute('SELECT * FROM words WHERE words.length = 6 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['mastermind_word'] = c.fetchone()[0]
        else:
            c.execute('SELECT * FROM words words.length >= 4 AND words.length <= 6 ORDER BY RANDOM() LIMIT 1;')
            CHANNELS[channel]['mastermind_word'] = c.fetchone()[0]
                
        CHANNELS[channel]['mastermind_guesses'] = []
        
        send_message(channel, 'Mastermind: I\'m thinking of a word with ' + str(len(CHANNELS[channel]['mastermind_word'])) + ' letters. :smile: = letter in the correct place, :neutral_face: = letter in the wrong place, :disappointed: = wrong letter. Position of the emoji does not necessarily match the position of the letter it refers to.')

def stop_mastermind(channel, sender):
    if channel in CHANNELS and CHANNELS[channel]['is_mastermind_running']:
        if sender == CHANNELS[channel]['mastermind_owner']:
            CHANNELS[channel]['is_mastermind_running'] = False
            send_message(channel, 'Game Ended. The word was: ' + CHANNELS[channel]['mastermind_word'] + '.')
        else:
            is_owner, is_admin = get_sender_permissions(sender)
            if is_owner or is_admin:
                CHANNELS[channel]['is_mastermind_running'] = False
                send_message(channel, 'Game Ended. The word was: ' + CHANNELS[channel]['mastermind_word'] + '.')
            else:
                send_message(channel, 'Sorry <@' + sender + '>, only <@' + CHANNELS[channel]['mastermind_owner'] + '> or an admin can end this game.')

def score_mastermind(channel, word):
    guess = list(word)
    mastermind_word = list(CHANNELS[channel]['mastermind_word'])
    
    correct_position = 0
    incorrect_position = 0
    incorrect_letters = 0
    
    for i in range(len(guess) - 1, -1, -1):
        if guess[i] == mastermind_word[i]:
            correct_position = correct_position + 1
            del(guess[i])
            del(mastermind_word[i])
    
    for c in guess:
        if c in mastermind_word:
            mastermind_word.remove(c)
            incorrect_position = incorrect_position + 1
        else:
            incorrect_letters = incorrect_letters + 1
    
    message = ''
    
    message = message + ':smile:' * correct_position
    message = message + ':neutral_face:' * incorrect_position
    message = message + ':disappointed:' * incorrect_letters
    
    return message.strip()
        

def display_math(channel):
    message = 'What is ' + CHANNELS[channel]['math_question'] + '?'
    send_message(channel, message)

def scramble_word(word):
    scramble = list(word)
    while True:
        random.shuffle(scramble)
        if ''.join(scramble) != word:
            return ''.join(scramble)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('DarwinBot requires 2 arguments: API_TOKEN and BOT_ID')
        sys.exit(1)
    
    API_TOKEN = sys.argv[1]
    BOT_ID = sys.argv[2]
    
    BOT_MENTION = '<@' + BOT_ID + '>'
    
    # REGEX
    BOT_DIRECT_PATTERN = re.compile('^' + re.escape(BOT_MENTION) + '(: ?| )(.+)$')
    BOT_MENTION_PATTERN = re.compile('^(.+) ' + re.escape(BOT_MENTION) + ' *$')
    
    log = open('darwinbot.log', 'a')
    
    slack_client = SlackClient(API_TOKEN)

    if slack_client.rtm_connect():
        print('DarwinBot connected and running!')
        
        while True:
            try:
                slack_output = slack_client.rtm_read()
                
                if slack_output and len(slack_output) > 0:
                    for output in slack_output:
                    
                        channel = get_channel(output) 
                        sender = get_sender(output)
                                            
                        if output:
                            if channel and sender and 'type' in output and output['type'] == 'message':
                                
                                is_command = False
                                is_direct = False
                                is_mention = False
                                
                                message = output['text'].strip()
                                
                                BOT_COMMAND_MATCH = BOT_COMMAND_PATTERN.search(message)
                                
                                if BOT_COMMAND_MATCH:
                                    is_command = True
                                    message = BOT_COMMAND_MATCH.group('command').strip()
                                else:
                                    BOT_DIRECT_MATCH = BOT_DIRECT_PATTERN.search(message)
                                    
                                    if BOT_DIRECT_MATCH:
                                        is_direct = True
                                        message = BOT_DIRECT_MATCH.group(2).strip()
                                    else:
                                        BOT_MENTION_MATCH = BOT_MENTION_PATTERN.search(message)
                                        if BOT_MENTION_MATCH:
                                            is_mention = True
                                            message = BOT_MENTION_MATCH.group(1).strip()
                                
                                lower_message = message.lower()
                                
                                if is_command:
                                    if message == 'restart':
                                        is_owner, is_admin = get_sender_permissions(sender)
                                        if is_owner or is_admin:
                                            send_message(channel, 'Restarting...')
                                            print('Restarting...')
                                            sys.exit(0)
                                    
                                    elif message == 'whoami':
                                        send_message(channel, '<@' + sender + '>: you are <@' + sender + '>, did you forget?')
                                    
                                    elif message == 'whoareyou':
                                        send_message(channel, '<@' + sender + '>: I am <@' + BOT_ID + '>!')
                                        
                                    elif message == 'flip':
                                        if random.randint(0, 1) == 0:
                                            send_message(channel, '<@' + sender + '>\'s coin twirls around in the air and comes down HEADS.')
                                        else:
                                            send_message(channel, '<@' + sender + '>\'s coin twirls around in the air and comes down TAILS.')
                                    elif message == 'dice' or message == 'roll':
                                        dice = 2
                                        sides = 6
                                        
                                        if BOT_COMMAND_MATCH.group('args'):
                                            params = BOT_COMMAND_MATCH.group('args').strip().split(' ')
                                            if len(params) >= 2:
                                                try:
                                                    dice = int(params[0])
                                                except ValueError as ex:
                                                    send_message(channel, '<@' + sender + '> ' + params[0] + ' isn\'t a valid integer!')
                                                    continue
                                                
                                                try:
                                                    sides = int(params[1])
                                                except ValueError as ex:
                                                    send_message(channel, '<@' + sender + '> ' + params[1] + ' isn\'t a valid integer!')
                                                    continue
                                            elif len(params) == 1:
                                                try:
                                                    dice = int(params[0])
                                                except ValueError as ex:
                                                    send_message(channel, '<@' + sender + '> ' + params[0] + ' isn\'t a valid integer!')
                                                    continue
                                        
                                        if dice < 1:
                                            send_message(channel, '<@' + sender + '> you can\'t have less than 1 die!')
                                            continue
                                        elif dice > 100:
                                            send_message(channel, '<@' + sender + '> you can\'t have more than 100 dice!')
                                            continue
                                        elif sides < 2:
                                            send_message(channel, '<@' + sender + '> you can\'t have fewer than 2 sides on your dice!')
                                            continue
                                        elif sides > 100:
                                            send_message(channel, '<@' + sender + '> you can\'t have more than 100 sides on your dice!')
                                            continue
                                        else:
                                            rolls = []
                                            
                                            for i in range(0, dice):
                                                rolls.append(random.randint(1, sides))
                                            
                                            send_message(channel, '<@' + sender + '> rolled: ' + ', '.join(map(str, rolls)))
                                    
                                    elif message == 'say':
                                        is_owner, is_admin = get_sender_permissions(sender)
                                        
                                        if is_owner:
                                            params = BOT_COMMAND_MATCH.group('args').strip().split(' ', 1)
                                            if len(params) == 2:
                                                RESPONSE_CHANNEL_SEARCH = re.search('^<#([A-Z0-9]+)\|', params[0])
                                                if RESPONSE_CHANNEL_SEARCH:
                                                    response_channel = RESPONSE_CHANNEL_SEARCH.group(1)
                                                    response_message = params[1]
                                                    send_message(response_channel, response_message)
                                    
                                    elif message == 'hangman':
                                        if BOT_COMMAND_MATCH.group('args'):
                                            if BOT_COMMAND_MATCH.group('args') == 'end' or BOT_COMMAND_MATCH.group('args') == 'stop':
                                                stop_hangman(channel, sender)
                                            elif BOT_COMMAND_MATCH.group('args') in ['easy', 'medium', 'hard']:
                                                start_hangman(channel, sender, BOT_COMMAND_MATCH.group('args'))
                                            else:
                                                start_hangman(channel, sender, 'any')
                                        else:
                                            start_hangman(channel, sender, 'any')
                                    
                                    elif message == 'mastermind':
                                        if BOT_COMMAND_MATCH.group('args'):
                                            if BOT_COMMAND_MATCH.group('args') == 'end' or BOT_COMMAND_MATCH.group('args') == 'stop':
                                                stop_mastermind(channel, sender)
                                            elif BOT_COMMAND_MATCH.group('args') in ['easy', 'medium', 'hard']:
                                                start_mastermind(channel, sender, BOT_COMMAND_MATCH.group('args'))
                                            elif BOT_COMMAND_MATCH.group('args') == 'recap':
                                                response_message = []
                                                
                                                for word in CHANNELS[channel]['mastermind_guesses']:
                                                    response_message.append(word + ': ' + score_mastermind(channel, word))
                                                
                                                send_message(channel, '\n'.join(response_message))
                                                
                                            else:
                                                start_mastermind(channel, sender, 'any')
                                        else:
                                            start_mastermind(channel, sender, 'any')
                                    
                                    elif message == 'anagram':
                                        if BOT_COMMAND_MATCH.group('args'):
                                            if BOT_COMMAND_MATCH.group('args') == 'end' or BOT_COMMAND_MATCH.group('args') == 'stop':
                                                stop_anagram(channel, sender)
                                            elif BOT_COMMAND_MATCH.group('args') == 'scramble' or BOT_COMMAND_MATCH.group('args') == 'shuffle':
                                                if channel in CHANNELS and CHANNELS[channel]['is_anagram_running']:
                                                    CHANNELS[channel]['anagram_scrambled_word'] = scramble_word(CHANNELS[channel]['anagram_scrambled_word'])
                                                    send_message(channel, 'Anagram: ' + CHANNELS[channel]['anagram_scrambled_word'])
                                            elif BOT_COMMAND_MATCH.group('args') == 'reveal' or BOT_COMMAND_MATCH.group('args') == 'clue':
                                                if channel in CHANNELS and CHANNELS[channel]['is_anagram_running']:
                                                    if CHANNELS[channel]['anagram_revealed'] < math.floor(len(CHANNELS[channel]['anagram_word']) / 2.0):
                                                        CHANNELS[channel]['anagram_revealed'] = CHANNELS[channel]['anagram_revealed'] + 1
                                                        
                                                        response_message = 'The first '
                                                        
                                                        if CHANNELS[channel]['anagram_revealed'] == 1:
                                                            response_message = response_message + 'letter of ' + CHANNELS[channel]['anagram_scrambled_word'] + ' is: '
                                                        else:
                                                            response_message = response_message + str(CHANNELS[channel]['anagram_revealed']) + ' letters of ' + CHANNELS[channel]['anagram_scrambled_word'] + ' are: ' 
                                                        
                                                        response_message = response_message + CHANNELS[channel]['anagram_word'][0:CHANNELS[channel]['anagram_revealed']]
                                                        
                                                        send_message(channel, response_message)
                                                    else:
                                                        send_message(channel, 'Sorry <@' + sender + '>, that\'s all I can give you.');
                                            elif BOT_COMMAND_MATCH.group('args') in ['easy', 'medium', 'hard']:
                                                start_anagram(channel, sender, BOT_COMMAND_MATCH.group('args'))
                                            else:
                                                start_anagram(channel, sender, 'any')
                                        else:
                                            start_anagram(channel, sender, 'any')
                                    
                                    elif message == 'math' or message == 'maths':
                                        if BOT_COMMAND_MATCH.group('args'):
                                            if BOT_COMMAND_MATCH.group('args') == 'end' or BOT_COMMAND_MATCH.group('args') == 'stop':
                                                if channel in CHANNELS and CHANNELS[channel]['is_math_running']:
                                                    if sender == CHANNELS[channel]['math_owner']:
                                                        CHANNELS[channel]['is_math_running'] = False
                                                        send_message(channel, 'Game Ended. ' + CHANNELS[channel]['math_question'] + ' = ' + str(CHANNELS[channel]['math_answer']))
                                                    else:
                                                        is_owner, is_admin = get_sender_permissions(sender)
                                                        if is_owner or is_admin:
                                                            CHANNELS[channel]['is_math_running'] = False
                                                            send_message(channel, 'Game Ended. ' + CHANNELS[channel]['math_question'] + ' = ' + str(CHANNELS[channel]['math_answer']))
                                                        else:
                                                            send_message(channel, 'Sorry <@' + sender + '>, only <@' + CHANNELS[channel]['math_owner'] + '> or an admin can end this game.')
                                        else:
                                            add_channel(channel)
                                            
                                            if CHANNELS[channel]['is_math_running']:
                                                send_message(channel, 'A math game is already running. Either complete the game or get <@' + CHANNELS[channel]['math_owner'] + '> or an admin to end the game. What is ' + CHANNELS[channel]['math_question'] + '?')
                                            else:
                                                CHANNELS[channel]['is_math_running'] = True
                                                CHANNELS[channel]['math_owner'] = sender
                                                
                                                rand = random.randint(0, 3)
                                                
                                                if rand == 0: # Addition
                                                    a = random.randint(0, 1000) + 1;
                                                    b = random.randint(0, 1000) + 1;
                                                    
                                                    CHANNELS[channel]['math_question'] = str(a) + ' + ' + str(b)
                                                    CHANNELS[channel]['math_answer'] = a + b
                                                    
                                                elif rand == 1: # Subtraction
                                                    a = random.randint(0, 1000) + 1;
                                                    b = random.randint(0, 1000) + 1;
                                                    
                                                    if a > b:
                                                        CHANNELS[channel]['math_question'] = str(a) + ' - ' + str(b)
                                                        CHANNELS[channel]['math_answer'] = a - b
                                                    else:
                                                        CHANNELS[channel]['math_question'] = str(b) + ' - ' + str(a)
                                                        CHANNELS[channel]['math_answer'] = b - a
                                                        
                                                elif rand == 2: # Multiplication
                                                    a = random.randint(0, 100) + 1;
                                                    b = random.randint(0, 100) + 1;
                                                    
                                                    CHANNELS[channel]['math_question'] = str(a) + ' * ' + str(b)
                                                    CHANNELS[channel]['math_answer'] = a * b
                                                    
                                                elif rand == 3:
                                                    CHANNELS[channel]['math_answer'] = random.randint(0, 100) + 1
                                                    
                                                    a = random.randint(0, 100) + 1
                                                    b = a * CHANNELS[channel]['math_answer']
                                                    
                                                    CHANNELS[channel]['math_question'] = str(b) + ' / ' + str(a)
                                                    
                                                display_math(channel)
                                    
                                    elif message == 'numberwang':
                                        if BOT_COMMAND_MATCH.group('args'):
                                            if BOT_COMMAND_MATCH.group('args') == 'end' or BOT_COMMAND_MATCH.group('args') == 'stop':
                                                if channel in CHANNELS and CHANNELS[channel]['is_numberwang_running']:
                                                    if sender == CHANNELS[channel]['numberwang_owner']:
                                                        CHANNELS[channel]['is_numberwang_running'] = False
                                                        send_message(channel, 'Game over! You\'ve been Wangernumbed!')
                                                    else:
                                                        is_owner, is_admin = get_sender_permissions(sender)
                                                        if is_owner or is_admin:
                                                            CHANNELS[channel]['is_numberwang_running'] = False
                                                            send_message(channel, 'Game over! You\'ve been Wangernumbed!')
                                                        else:
                                                            send_message(channel, 'Sorry <@' + sender + '>, only <@' + CHANNELS[channel]['numberwang_owner'] + '> or an admin can end this game.')
                                        else:
                                            add_channel(channel)
                                            if CHANNELS[channel]['is_numberwang_running']:
                                                send_message(channel, 'A Numberwang game is already running. Either complete the game or get <@' + CHANNELS[channel]['numberwang_owner'] + '> or an admin to end the game.')
                                            else:
                                                CHANNELS[channel]['is_numberwang_running'] = True
                                                CHANNELS[channel]['numberwang_owner'] = sender
                                                
                                                send_message(channel, 'Let\'s play Numberwang!')
                                    
                                    elif message == 'magic8ball' or message == '8ball':
                                        send_message(channel, random.choice(BOT_8BALL_RESPONSES))
                                    
                                    elif message == 'define':
                                        if BOT_COMMAND_MATCH.group('args'):
                                            word = urllib.parse.quote(BOT_COMMAND_MATCH.group('args').strip(), safe='')
                                            
                                            r = requests.get('https://aplet123-wordnet-search-v1.p.mashape.com/master', params={
                                                'word': BOT_COMMAND_MATCH.group('args').strip(),
                                            }, headers = {
                                                'X-Mashape-Key': '6idnTOB6L9mshmg9BrZLiqmN7BZdp1EmPEdjsnRyBEO8Cmb9yx',
                                                'Accept': 'text/plain',
                                            })
                                            
                                            if r.status_code == 200:
                                                results = r.json()
                                                
                                                if 'error' in results and results['error'] == None and 'definition' in results and len(results['definition']) > 0:
                                                    response_message = ''
                                                    definitions = [x for x in results['definition'].split('\nS: ') if x]
                                                    
                                                    i = 1
                                                    for definition in definitions[:3]:
                                                        if len(definitions[:3]) != 1:
                                                            response_message = response_message + str(i) + '. '
                                                        
                                                        response_message = response_message + definition
                                                        
                                                        if len(definitions[:3]) != 1 and i != len(definitions[:3]):
                                                            response_message = response_message + '\n'
                                                        
                                                        i = i + 1
                                                    
                                                    send_message(channel, response_message)
                                                    
                                                else:
                                                    send_message(channel, 'I\'m sorry, <@' + sender + '>. I don\'t seem to have a definition of "' + BOT_COMMAND_MATCH.group('args') + '".')
                                            else:
                                                send_message(channel, 'There appears to be an issue with the Dictionary API. Please try again later.')
                                            
                                            '''
                                            page = urllib.request.urlopen('http://www.abbreviations.com/services/v2/defs.php?uid=2136&tokenid=cv9SzwQIVumeYdzQ&word=' + word).read()
                                            soup = BeautifulSoup(page, 'lxml-xml')
                                            
                                            results = soup.select('result')
                                            
                                            response_message = ''
                                            
                                            if len(results) > 0:
                                                i = 1
                                                for result in results[:3]:
                                                    if len(results) != 1:
                                                        response_message = response_message + str(i) + '. '
                                                    
                                                    response_message = response_message + result.term.string + ' '
                                                    
                                                    if result.partofspeech:
                                                        response_message = response_message + '(' + result.partofspeech.string.lower() + ') '
                                                    
                                                    response_message = response_message + '-- ' + urllib.parse.unquote(result.definition.string)
                                                    
                                                    if len(results) != 1 and i != len(results):
                                                        response_message = response_message + '\n'
                                                    
                                                    i = i + 1
                                                
                                                send_message(channel, response_message)
                                            else:
                                                send_message(channel, 'I\'m sorry, <@' + sender + '>. I don\'t seem to have a definition of "' + BOT_COMMAND_MATCH.group('args') + '".')
                                            '''
                                    
                                    elif message == 'meth':
                                        add_channel(channel)
                                        response_message = random.choice(BOT_METH_RESPONSES)
                                        
                                        if response_message == 'Say my name.':
                                            CHANNELS[channel]['meth_say_my_name'] = True
                                        
                                        send_message(channel, response_message)
                                    
                                    elif message == 'troll':
                                        send_message(channel, 'TROLOLOLOLOL')
                                                    
                                        
                                elif is_direct or is_mention:
                                    
                                    # Patterns which apply to both direct messages and mentions:
                                    if HELLO_PATTERN.search(message):
                                        send_message(channel, random.choice(BOT_HELLO_RESPONSES).replace('{1}', '<@' + sender + '>'))
                                    
                                    elif FAREWELL_PATTERN.search(message):
                                        send_message(channel, random.choice(BOT_FAREWELL_RESPONSES).replace('{1}', '<@' + sender + '>'))
                                    
                                    elif THANKS_PATTERN.search(message):
                                        send_message(channel, random.choice(BOT_THANKS_RESPONSES).replace('{1}', '<@' + sender + '>'))
                                    
                                    elif INSULT_PATTERN.search(message):
                                        send_message(channel, random.choice(BOT_INSULT_RESPONSES).replace('{1}', '<@' + sender + '>'))
                                    
                                    elif LIKE_PATTERN.search(message):
                                        LIKE_MATCH = LIKE_PATTERN.search(message)
                                        send_message(channel, 'I ' + LIKE_MATCH.group(1).lower() + ' you too, <@' + sender + '>')
                                    
                                    else:
                                        # Patterns which apply to direct messages only:
                                        if is_direct:
                                            
                                            if GOD_PATTERN.search(message):
                                                send_message(channel, random.choice(BOT_GOD_RESPONSES))
                                            elif lower_message == 'make me a sandwich':
                                                send_message(channel, 'What? Make it yourself.')
                                            elif lower_message == 'sudo make me a sandwich':
                                                send_message(channel, 'Okay.')
                                            elif HAL_PATTERN.search(message):
                                                send_message(channel, 'I\'m sorry, Dave. I\'m afraid I can\'t do that.')
                                            elif lower_message == 'global thermonuclear war':
                                                if random.randint(0, 1) == 0:
                                                    send_message(channel, 'Wouldn\'t you prefer a nice game of chess?')
                                                else:
                                                    send_message(channel, 'A strange game. The only winning move is not to play. How about a nice game of chess?')
                                            elif HANGMAN_LETTER_PATTERN.search(message):
                                                hangman_guess(channel, sender, message)
                                                
                                        # Patterns which apply to mentions only:
                                        elif is_mention:
                                            test = 'test'
                                else:
                                    if HANGMAN_LETTER_PATTERN.search(message):
                                        hangman_guess(channel, sender, message)
                                
                                    if channel in CHANNELS and CHANNELS[channel]['is_hangman_running'] and CHANNELS[channel]['hangman_word'].lower() == lower_message:
                                        CHANNELS[channel]['is_hangman_running'] = False
                                        send_message(channel, 'Congratulations <@' + sender + '>! The word was: ' + CHANNELS[channel]['hangman_word'] + '.')
                                    
                                    if channel in CHANNELS and CHANNELS[channel]['is_anagram_running'] and CHANNELS[channel]['anagram_word'].lower() == lower_message:
                                        CHANNELS[channel]['is_anagram_running'] = False
                                        send_message(channel, 'Congratulations <@' + sender + '>! The anagram was: ' + CHANNELS[channel]['anagram_word'] + '.')
                                        
                                    if channel in CHANNELS and CHANNELS[channel]['is_mastermind_running'] and MASTERMIND_WORD_PATTERN.search(message):
                                        if CHANNELS[channel]['mastermind_word'].lower() == lower_message:
                                            CHANNELS[channel]['is_mastermind_running'] = False
                                            send_message(channel, 'Congratulations <@' + sender + '>! The mastermind word was: ' + CHANNELS[channel]['mastermind_word'] + '.')
                                        elif len(CHANNELS[channel]['mastermind_word']) == len(lower_message):
                                            if lower_message not in CHANNELS[channel]['mastermind_guesses']:
                                                mastermind_word_guess = (lower_message,)
                                                c.execute('SELECT COUNT(*) FROM words WHERE words.word=?', mastermind_word_guess)
                                                result = c.fetchone()
                                                
                                                if result[0] == 1:
                                                    CHANNELS[channel]['mastermind_guesses'].append(lower_message)
                                                    send_message(channel, lower_message + ': ' + score_mastermind(channel, lower_message))
                                    
                                    if channel in CHANNELS and CHANNELS[channel]['is_math_running'] and MATH_ANSWER_PATTERN.search(message) and message == str(CHANNELS[channel]['math_answer']):
                                        CHANNELS[channel]['is_math_running'] = False
                                        send_message(channel, 'Congratulations <@' + sender + '>! ' + CHANNELS[channel]['math_question'] + ' = ' + str(CHANNELS[channel]['math_answer']))
                                    
                                    if channel in CHANNELS and CHANNELS[channel]['meth_say_my_name'] and (message.strip().lower() == BOT_NAME or message.strip() == BOT_MENTION):
                                        CHANNELS[channel]['meth_say_my_name'] = False
                                        send_message(channel, 'You\'re goddamn right!')
                                    
                                    if channel in CHANNELS and CHANNELS[channel]['is_numberwang_running'] and NUMBERWANG_ANSWER_PATTERN.search(message):
                                        rand = random.randint(1, 4)
                                        
                                        if rand == 1:
                                            CHANNELS[channel]['is_numberwang_running'] = False
                                            if random.randint(0, 1) == 0:
                                                send_message(channel, '<@' + sender + '>: That\'s Numberwang!')
                                            else:
                                                send_message(channel, '<@' + sender + '>: That\'s the Numberwang bonus! Triple points!')
                                        elif rand == 2:
                                            send_message(channel, 'Let\'s rotate the board!')
                                        elif rand == 3:
                                            send_message(channel, 'Sorry {1}, that is not a number.'.replace('{1}', '<@' + sender + '>'))
                                        elif rand == 4:
                                            send_message(channel, 'Bad luck {1}, you\'ve been Wangernumbed!'.replace('{1}', '<@' + sender + '>'))
                                                                        
                time.sleep(WEBSOCKET_DELAY)
            except WebSocketConnectionClosedException:
                print('Connection Closed Unexpectedly. Reconnecting...')
                slack_client.rtm_connect()
                
    else:
        print('Connection failed. Invalid Slack token or bot ID?')
