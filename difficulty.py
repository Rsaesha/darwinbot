from pprint import pprint
from collections import defaultdict
from string import ascii_lowercase
import random
import sys

WORDS = {}

def partition(guess, words):
    """Apply the single letter 'guess' to the sequence 'words' and return
    a dictionary mapping the pattern of occurrences of 'guess' in a
    word to the list of words with that pattern.

    >>> words = 'deed even eyes mews peep star'.split()
    >>> sorted(list(partition('e', words).items()))
    [(0, ['star']), (2, ['mews']), (5, ['even', 'eyes']), (6, ['deed', 'peep'])]

    """
    result = defaultdict(list)
    for word in words:
        key = sum(1 << i for i, letter in enumerate(word) if letter == guess)
        result[key].append(word)
    return result

def guess_cost(guess, words):
    """Return the cost of a guess, namely the number of words that don't
    contain the guess.

    >>> words = 'deed even eyes mews peep star'.split()
    >>> guess_cost('e', words)
    1
    >>> guess_cost('s', words)
    3

    """
    return sum(guess not in word for word in words)

def word_guesses(words, wrong = 0, letters = ''):
    """Given the collection 'words' that match all letters guessed so far,
    generate tuples (wrong, nguesses, word, guesses) where
    'word' is the word that was guessed;
    'guesses' is the sequence of letters guessed;
    'wrong' is the number of these guesses that were wrong;
    'nguesses' is len(guesses).

    >>> words = 'deed even eyes heel mere peep star'.split()
    >>> from pprint import pprint
    >>> pprint(sorted(word_guesses(words)))
    [(0, 1, 'mere', 'e'),
     (0, 2, 'deed', 'ed'),
     (0, 2, 'even', 'en'),
     (1, 1, 'star', 'e'),
     (1, 2, 'eyes', 'en'),
     (1, 3, 'heel', 'edh'),
     (2, 3, 'peep', 'edh')]

    """
    if len(words) == 1:
        yield wrong, len(letters), words[0], letters
        return
    
    #try:
    #    best_guess = min((g for g in ascii_lowercase if g not in letters), key = lambda g:guess_cost(g, words))
    #except ValueError:
    #    yield wrong, len(letters), words[0], letters
    #    return
    best_guess = min((g for g in ascii_lowercase if g not in letters), key = lambda g:guess_cost(g, words))
        
    best_partition = partition(best_guess, words)
    letters += best_guess
    for pattern, words in best_partition.items():
        for guess in word_guesses(words, wrong + (pattern == 0), letters):
            yield guess

wordlist = [line.strip() for line in open('words.txt')]

hangman_difficulties = word_guesses(wordlist)

#print('Organising Wordlist')

for word in hangman_difficulties:
    length = len(word[2])
    hangman_difficulty = word[0]
    print(word[2] + ',' + str(length) + ',' + str(hangman_difficulty))

#for word in wordlist:
#    length = len(word)
#    hangman_difficulty = difficulty(word)
#    print(word + ',' + str(length) + ',' + str(hangman_difficulty))

#print('Done')

#print('Dictionary Size: ' + str(sys.getsizeof(WORDS)))


#pprint(WORDS)
#print('Searching for 8+ char words')
#print(random.choice([k for k, v in WORDS.items() if v['l'] == 8]))
